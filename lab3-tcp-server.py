import socket
import os
import logging.handlers

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
my_ip_address = '127.0.0.1'
my_port = 12345
hostname = socket.gethostname()
processID = os.getpid()
remote_host_IP = '192.168.180.158'

sock.bind((my_ip_address, my_port))
sock.listen(5)
my_logger = logging.getLogger('MyLogger')
my_logger.setLevel(logging.DEBUG)
handler = logging.handlers.SysLogHandler(address=(remote_host_IP, 514))
my_logger.addHandler(handler)

logging.basicConfig(format='%(asctime)s lab03lnx %(message)s', level=logging.INFO, filename='/var/log/syslog',
                    datefmt='%b %d %H:%M:%S')

my_logger.info(f"{hostname} Server started listening on port {my_port}. IP address: {my_ip_address}. Process ID: {processID}")
print(f"{hostname} Server started listening on port {my_port}. IP address: {my_ip_address}. Process ID: {processID}")

while True:
    connection, address = sock.accept()
    address = (str(address).split(',')[0][2:-1])
    connection.send(b'Welcome to a simple TCP server, enter your message: \n\n\n')

    my_logger.info(f"{hostname} lab3-tcp-server.py[{processID}] Successfully established connection with IP address: {address}")
    print(f"{hostname} lab3-tcp-server.py[{processID}] Successfully established connection with IP address: {address}")

    message = connection.recv(1024)
    decoded = message.decode('utf8')[:-1]
    my_logger.info(f"{hostname} lab3-tcp-server.py[{processID}] Successfully got message from IP {address}: {decoded}")
    print(f"{hostname} lab3-tcp-server.py[{processID}] Successfully got message from IP {address}: {decoded}")

    connection.send(b'\n' + message)
    my_logger.info(f"{hostname} lab3-tcp-server.py[{processID}] Successfully send reply to IP {address}: {decoded}")
    print(f"{hostname} lab3-tcp-server.py[{processID}] Successfully send reply to IP {address}: {decoded}")

    connection.send(b'\n\nThank you! Connection is closed. Have a nice day!')
    connection.close()
    my_logger.info(f"{hostname} lab3-tcp-server.py[{processID}] Successfully finished connection with IT {address}")
    print(f"{hostname} lab3-tcp-server.py[{processID}] Successfully finished connection with IT {address}")
