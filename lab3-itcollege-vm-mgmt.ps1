# Lab Management script for ICS0020
# v1 / 2019.09.11
#       main functionality implemented, script used to provision teacher and students labs
#       check MAIN BODY for the logic

# documentation https://code.vmware.com/web/tool/11.3.0/vmware-powercli

#Adding parameter for credential input
Param(
    [Parameter(Mandatory=$false, Position=0, HelpMessage="Remote Host IP/DNS?: vcsa.itcollege.ee (default)")]
    [String]$Hostname = 'vcsa.itcollege.ee',

    [Parameter(Mandatory=$true, Position=1, HelpMessage="User?")]
    [String]$User,

    [Parameter(Mandatory=$true, Position=2, HelpMessage="Password?")]
    [SecureString]$EncryptedPassword,

    [Parameter(Mandatory=$false, Position=3, HelpMessage="Remote SSH Port: 22 (default)")]
    [String]$RemoteHostPort = 22
)

# Virtual Center connection parameters
$vc = @{ `
    Host                 = $Hostname; `
    User                 = $User;` 
    Passwd = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($EncryptedPassword))`
    }


# Resource pool definition
$pool = @{ `
    Compute             = 'HPE BladeSystem Gen8 - Rack 3'; `
    Storage             = 'Hitachi_LUN1'; `
    Network             = 'VM Network'; `
    BaseLabLocation     = 'Labs.Students'; `
    BaseLabLocation2    = 'Labs.Course Development' `
    }

# List of templates
$tmpl = @{ `
    Lnx                 = 'Ubuntu 18.04 basic template v1.1'; `
    Win                 = 'Windows Server 2019 basic template v1.1'; `
}

# VM attributes
$vm = @{ `
    prfx                = 'ics0020-lab'; `
    WinTarget01         = 'managed-Node-Win-01'; `
    LnxTarget01         = 'managed-Node-Lnx-01'; `
    MgmtSrv01           = 'management-Server-01' `
}

$hostname = hostname
$syslogserver = "192.168.180.158"
# Uncomment and run once to configure the environment
#Install-Module -Name VMware.PowerCLI
#Set-PowerCLIConfiguration -Scope User -ParticipateInCEIP $false
#Set-PowerCLIConfiguration -InvalidCertificateAction Ignore

Try
{
    Connect-VIServer -Server $vc.Host -User $vc.User -Pass $vc.Passwd
}

Catch
{
    Write-Error -Message "Credential is wrong. Try again" -ErrorAction Stop
}

$objtmpl = @{ `
    Lnx = Get-Template -Name $tmpl.Lnx; `
    Win = Get-Template -Name $tmpl.Win `
    }

$objclstDefault = Get-Cluster -Name $pool.Compute
#$objstrgDefault = Get-Datastore -Name $pool.Storage

Function GetLabVMsIPs {
    For ($i=$a; $i -le $z; $i++) {
        $lab = $vm.prfx + "{0:00}" -f $i
        #Get-VM ($lab + "*") | Select-Object Name, @{N="IP Address";E={@($_.guest.IPAddress[0])}} | Sort-Object Name | ConvertTo-Csv
        $base = Get-VM ($lab + "*") | Select Name, @{N="IP";E={@($_.guest.IPAddress[0])}}
        For ($j = 0; $j -le $vmsnumber-1; $j++)
        {
                if($base[$j].IP -eq $null -Or $base[$j].Name -eq $null)
                {
                        Send-SyslogMessage -Server "$syslogserver" -Facility 1 -Severity 3 -ApplicationName "lab3-itcollege-vm-mgmt.ps" -Hostname "$hostname" -Message "Warning: Couldn't get the name and IP address of VM. VCSA service structure might have changed or script is not up to date"
                        echo "Couldn't get the name and IP address of VM. VCSA service structure might have changed or script is not up to date"
                } else
                {
                        $vmName = $base[$j].Name
                        $vmIP = $base[$j].IP
                        Send-SyslogMessage -Server "$syslogserver" -Facility 1 -Severity 6 -ApplicationName "lab3-itcollege-vm-mgmt.ps" -Hostname "$hostname" -Message "Successfuly got $vmName with IP $vmIP"
                        echo "Successfuly got $vmName with IP $vmIP"
                }
        }
    }
}

Function PingLabVMsIPs {
    For ($i=$a; $i -le $z; $i++) {
        $lab = $vm.prfx + "{0:00}" -f $i
        $base = Get-VM ($lab + "*") | Select Name, @{N="IP";E={@($_.guest.IPAddress[0])}}
        For ($j = 0; $j -le $vmsnumber-1; $j++)
        {
                if($base[$j].IP -eq $null -Or $base[$j].Name -eq $null)
                {
                        Send-SyslogMessage -Server "$syslogserver" -Facility 1 -Severity 3 -ApplicationName "lab3-itcollege-vm-mgmt.ps" -Hostname "$hostname" -Message "Warning: Couldn't get an IP address of VM. Ping was unsuccessful"
                        echo "Couldn't get an IP address of VM. Ping was unsuccessful"
                } else
                {
                        $vmName = $base[$j].Name
                        $vmIP = $base[$j].IP
                        if (Test-Connection -IPv4 $vmIP -Quiet)
                        {
                                Send-SyslogMessage -Server "$syslogserver" -Facility 1 -Severity 6 -ApplicationName "lab3-itcollege-vm-mgmt.ps" -Hostname "$hostname" -Message "Ping to $vmName with IP $vmIP was successful"
                                echo "Ping to $vmName with IP $vmIP was successful"
                        } else {
                                Send-SyslogMessage -Server "$syslogserver" -Facility 1 -Severity 3 -ApplicationName "lab3-itcollege-vm-mgmt.ps" -Hostname "$hostname" -Message "Warning: Ping to $vmName with IP $vmIP was unsuccessful"
                                echo "Warning: Ping to $vmName with IP $vmIP was unsuccessful"
                        }
                }
        }
    }
}

Function RemoveLabVMs {
    For ($i=$a; $i -le $z; $i++) {
        $lab = $vm.prfx + "{0:00}" -f $i
        Remove-Folder -Folder $lab -DeletePermanently -Confirm:$false
    }
}

Function StartLabVMs {
    For ($i=$a; $i -le $z; $i++) {
        $lab = $vm.prfx + "{0:00}" -f $i

        $labMgmtSrv01   = $lab + '.' + $vm.MgmtSrv01
        $labLnxTarget01 = $lab + '.' + $vm.LnxTarget01
        $labWinTarget01 = $lab + '.' + $vm.WinTarget01

        Start-VM -VM $labMgmtSrv01 -RunAsync
        Start-VM -VM $labLnxTarget01 -RunAsync
        Start-VM -VM $labWinTarget01 -RunAsync
    }
}

Function StopLabVMs {
    For ($i=$a; $i -le $z; $i++) {
        $lab = $vm.prfx + "{0:00}" -f $i

        $labMgmtSrv01   = $lab + '.' + $vm.MgmtSrv01
        $labLnxTarget01 = $lab + '.' + $vm.LnxTarget01
        $labWinTarget01 = $lab + '.' + $vm.WinTarget01

        Stop-VM -VM $labMgmtSrv01 -RunAsync -Confirm:$false
        Stop-VM -VM $labLnxTarget01 -RunAsync -Confirm:$false
        Stop-VM -VM $labWinTarget01 -RunAsync -Confirm:$false
    }
}

Function ProvisionLabVMs {
    For ($i=$a; $i -le $z; $i++) {
        $lab = $vm.prfx + "{0:00}" -f $i
        New-Folder -Name $lab -Location $pool.BaseLabLocation

        $labMgmtSrv01   = $lab + '.' + $vm.MgmtSrv01
        $labLnxTarget01 = $lab + '.' + $vm.LnxTarget01
        $labWinTarget01 = $lab + '.' + $vm.WinTarget01

        New-VM -Name $labMgmtSrv01 `
            -Template $objtmpl.Lnx `
            -ResourcePool $objclstDefault `
            -Datastore $objstrgDefault `
            -Location $lab

        New-VM -Name $labLnxTarget01 `
            -Template $objtmpl.Lnx `
            -ResourcePool $objclstDefault `
            -Datastore $objstrgDefault `
            -Location $lab

            New-VM -Name $labWinTarget01 `
            -Template $objtmpl.Win `
            -ResourcePool $objclstDefault `
            -Datastore $objstrgDefault `
            -Location $lab
    }
}

########################### MAIN BODY ########################
# Set a and z parameters to set lab range, use same values to work with single lab vms
# Uncomment functions you want to run

$a = 1
$z = 30
$vmsnumber = 4

#StopLabVMs
#RemoveLabVMs
#ProvisionLabVMs
#StartLabVMs
GetLabVMsIPs
PingLabVMsIPs